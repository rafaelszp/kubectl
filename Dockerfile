FROM alpine:3.6

LABEL MAINTAINER="Rafael Pereira <rafaelszp.info@gmail.com>"

ENV KUBE_LATEST_VERSION="v1.18.3"

RUN apk add --update ca-certificates \
    && apk add --update -t deps curl \
    && apk add --update gettext git bash \
    && curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl 

RUN curl -sL https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2Fv4.0.4/kustomize_v4.0.4_linux_amd64.tar.gz -o k.tar.gz \
    && tar xzf ./k.tar.gz \
    && mv ./kustomize /usr/local/bin/

RUN apk add --update -t recode
    
#RUN apk del --purge deps \
RUN rm /var/cache/apk/*
